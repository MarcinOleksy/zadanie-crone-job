<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'supervisor_user_id',
        'street',
        'building_number',
        'city',
        'zip'
    ];

    public $timestamps = false;

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [];

    public function updateAllWhereUserIdToAnotherUserId(int $change_from_user_id, int $change_to_user_id)
    {
        return Estate::where('supervisor_user_id','=', $change_from_user_id)->update(['supervisor_user_id'=> $change_to_user_id]);
    }
}
