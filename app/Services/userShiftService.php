<?php

namespace App\Services;

use App\Models\UserShift;
use App\Models\Estate;
use \DateTime as DateTime;

class userShiftService
{
	private $dateExecution;
	private $dateFormat = "Y-m-d";

	public function __construct(DateTime $date = null)
	{
		$this->generateAndSetDate($date);
	}

	public function changeUserIdInEstateBasedOnUserShift()
	{
		$usersShifts = $this->importUsersShifts();

		foreach($usersShifts as $userShift)
			$this->updateAllEstatesWhereDataFromAndDataToIsDataExecution($userShift);
	}

	private function updateAllEstatesWhereDataFromAndDataToIsDataExecution(UserShift $userShift)
	{
		if($userShift->date_from == $this->dateExecution)
			$this->updateUserIdToSubstituteUserIdInAllEstates($userShift);
		elseif($userShift->date_to == $this->dateExecution)
			$this->updateSubstituteUserIdToUserIdInAllEstates($userShift);
	}

	private function updateUserIdToSubstituteUserIdInAllEstates(UserShift $userShift)
	{
		Estate::updateAllWhereUserIdToAnotherUserId($userShift->user_id, $userShift->substitute_user_id);
	}

	private function updateSubstituteUserIdToUserIdInAllEstates(UserShift $userShift)
	{
		Estate::updateAllWhereUserIdToAnotherUserId($userShift->substitute_user_id, $userShift->user_id);
	}

	private function importUsersShifts()
	{
		return UserShift::findAllWhereDateFromOrDateTo($this->dateExecution);
	}

	private function generateAndSetDate(DateTime $date = null)
	{
		if(!$date)
			$date = new DateTime();

		$dateStringFormat = $this->changeDateToStringFormat($date);

		return $this->setDate($dateStringFormat);
	}

	private function changeDateToStringFormat(DateTime $date = null)
	{
		return $date->format($this->dateFormat);
	}

	private function setDate(string $date)
	{
		$this->dateExecution = $date;
	}
}