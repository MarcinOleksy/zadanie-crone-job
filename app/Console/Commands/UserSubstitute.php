<?php 

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserShiftService;
use \DateTime as DateTime;

class UserSubstitute extends Command
{
	protected $signature = "substituting:users {data?}";

	protected $description = "Replacing inaccessible users in housing estates.";

    public function handle()
    {
        $date = null;
        $argumentData = $this->argument('data');

        if($argumentData)
            $date = new DateTime($argumentData);

        $userShiftService = new UserShiftService($date);
        $userShiftService->changeUserIdInEstateBasedOnUserShift();
    }
}